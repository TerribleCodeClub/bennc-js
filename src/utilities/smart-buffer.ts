import { numberToUint16BE, numberToUint32BE } from './number'

export class SmartBuffer {
  private _data: number[]
  private _cursor: number

  /**
  * Wrap a buffer to track position and provide useful read / write functionality.
  * @param data Buffer to wrap (optional).
  */
  constructor (length: number = 0) {
    this._data = new Array<number>(length)
    this._cursor = 0
  }

  /**
  * Return a regular buffer.
  */
  get data (): Uint8Array {
    return new Uint8Array(this._data)
  }

  /**
  * Update the smart buffer to wrap new data.
  */
  set data (data: Uint8Array) {
    this._data = Array.from(data)
    this.cursor = 0
  }

  /**
  * Get the length of the smart buffer data.
  */
  get length (): number {
    return this._data.length
  }

  /**
  * Set the length of the smart buffer data.
  */
  set length (length: number) {
    this._data.length = length
  }

  /**
  * Get the current cursor position of the smart buffer.
  */
  get cursor (): number {
    return this._cursor
  }

  /**
  * Set the cursor position of the smart buffer.
  */
  set cursor (position: number) {
    if (position < 0) {
      throw RangeError(`Cannot seek to ${this.cursor} of ${this.length} bytes.`)
    }
    this._cursor = position
  }

  /**
   * Create a new SmartBuffer from an existing object.
   * @param data The object to convert to a new SmartBuffer.
   * @returns A new SmartBuffer.
   */
  static from (data: number[] | ArrayBuffer): SmartBuffer {
    const smartBuffer = new SmartBuffer()
    if (data instanceof ArrayBuffer) {
      smartBuffer._data = Array.from(new Uint8Array(data))
    } else {
      smartBuffer._data = Array.from(data)
    }
    return smartBuffer
  }

  /**
  * Pads bytes to the end of the smart buffer.
  * @param length The number of bytes to pad.
  */
  pad (length: number): void {
    this._data.push(...Array<number>(length).fill(0))
    this.cursor += length
  }

  /**
  * Return the data from the specified range.
  * @param start The start position.
  * @param end The end position.
  * @returns A new buffer containing data from the specified range.
  */
  slice (start: number, end: number): Uint8Array {
    return new Uint8Array(this._data.slice(start, end))
  }

  /**
   * Splice bytes into the smart buffer's data.
   * @param start The position at which to insert the new data.
   * @param deleteCount The number of items to remove before inserting new data.
   * @param items The items to insert at the specified position.
   */
  splice (start: number, deleteCount: number, ...items: number[]): void {
    if (this.length < start) {
      this._data.push(...Array<number>(start))
    }
    this._data.splice(this.cursor, deleteCount, ...items)
  }

  /**
  * Read a UInt16 number from the smart buffer at the current cursor position, and increment the cursor.
  * @returns A number represented by the bytes at the current cursor position.
  */
  readUInt16 (): number {
    const num = this.slice(this.cursor, this.cursor + 2)
    this.cursor += 2
    return (num[0] << 8 | num[1]) >>> 0
  }

  /**
  * Read a UInt32 number from the smart buffer at the current cursor position, and increment the cursor.
  * @returns A number represented by the bytes at the current cursor position.
  */
  readUInt32 (): number {
    const num = this.slice(this.cursor, this.cursor + 4)
    this.cursor += 4
    return (num[0] << 24 | num[1] << 16 | num[2] << 8 | num[3]) >>> 0
  }

  /**
  * Read the specified number of bytes from the smart buffer at the current cursor position, and increment the cursor.
  * @returns A buffer containing the bytes read from the current cursor position.
  */
  readBytes (length: number): Uint8Array {
    this.cursor += length
    return this.slice(this.cursor - length, this.cursor)
  }

  /**
  * Write a UInt16 number to the smart buffer at the current cursor position, and increment the cursor.
  * @param number The number to write in UInt16 format at the current cursor position.
  */
  writeUInt16 (number: number): void {
    this.splice(this.cursor, 2, ...numberToUint16BE(number))
    this.cursor += 2
  }

  /**
  * Write a UInt32 number to the smart buffer at the current cursor position, and increment the cursor.
  * @param number The number to write in UInt32 format at the current cursor position.
  */
  writeUInt32 (number: number): void {
    this.splice(this.cursor, 4, ...numberToUint32BE(number))
    this.cursor += 4
  }

  /**
  * Write the specified bytes to the smart buffer at the current cursor position, and increment the cursor.
  * @param buffer The bytes to write at the current cursor position.
  */
  writeBytes (buffer: number[] | Uint8Array): void {
    this.splice(this.cursor, buffer.length, ...buffer)
    this.cursor += buffer.length
  }
}
