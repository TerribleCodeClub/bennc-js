/**
 * Pack a number to a 2 byte Uint16 buffer (big endian).
 * @param number The number to pack.
 * @returns The packed buffer.
 */
export function numberToUint16BE (number: number): Uint8Array {
  const ret = new Uint8Array(2)
  ret[0] = (number & 0xFF00) >> 8
  ret[1] = (number & 0x00FF) >> 0
  return ret
}

/**
 * Pack a number to a 4 byte Uint32 buffer (big endian).
 * @param number The number to pack.
 * @returns The packed buffer.
 */
export function numberToUint32BE (number: number): Uint8Array {
  const ret = new Uint8Array(4)
  ret[0] = (number & 0xFF000000) >> 24
  ret[1] = (number & 0x00FF0000) >> 16
  ret[2] = (number & 0x0000FF00) >> 8
  ret[3] = (number & 0x000000FF) >> 0
  return ret
}
