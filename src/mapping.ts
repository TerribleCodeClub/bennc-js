import { packSubscribeMessage } from "./messages/subscribe";
import { packBasicMessage, unpackBasicMessage } from "./messages/basic";
import {
  packUserDataRequestMessage,
  unpackUserDataRequestMessage,
} from "./messages/userDataRequest";
import {
  packUserDataResponseMessage,
  unpackUserDataResponseMessage,
} from "./messages/userDataResponse";
import { packKeepaliveMessage } from "./messages/keepalive";
import { packUnsubscribeMessage } from "./messages/unsubscribe";
import { packGetHistoryMessage } from "./messages/history";

export const packers = {
  0x0000: packSubscribeMessage,
  0x0001: packBasicMessage,
  0x0002: packUserDataRequestMessage,
  0x0003: packUserDataResponseMessage,
  0x0005: packKeepaliveMessage,
  0xfffe: packGetHistoryMessage,
  0xffff: packUnsubscribeMessage,
};

export const unpackers = {
  0x0001: unpackBasicMessage,
  0x0002: unpackUserDataRequestMessage,
  0x0003: unpackUserDataResponseMessage,
};
