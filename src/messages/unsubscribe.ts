import { MessageTypes } from '../common'
import { numberToUint16BE } from '../utilities/number'
import { packOutgoingPacket } from './packet'

const MESSAGE_TYPE = numberToUint16BE(MessageTypes.Unsubscribe)

export interface UnsubscribeMessage {
  messageType: number
}

/**
 * Create an outgoing unsubscribe (0xFFFF) packet.
 * @param properties The properties for the message.
 * @returns An outgoing unsubscribe (0xFFFF) packet.
 */
export function packUnsubscribeMessage (properties: UnsubscribeMessage): Uint8Array {
  const data = numberToUint16BE(properties.messageType)
  return packOutgoingPacket({
    messageType: MESSAGE_TYPE,
    data: data
  })
}
