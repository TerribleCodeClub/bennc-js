import { MessageTypes } from '../common'
import { numberToUint16BE } from '../utilities/number'
import { packOutgoingPacket } from './packet'

const MESSAGE_TYPE = numberToUint16BE(MessageTypes.Keepalive)

/**
 * Create an outgoing keepalive (0x0005) packet.
 * @returns An outgoing keepalive (0x0005) packet.
 */
export function packKeepaliveMessage (): Uint8Array {
  return packOutgoingPacket({
    messageType: MESSAGE_TYPE,
    data: new Uint8Array(0)
  })
}
