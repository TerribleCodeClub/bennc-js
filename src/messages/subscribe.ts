import { MessageTypes } from '../common'
import { numberToUint16BE } from '../utilities/number'
import { packOutgoingPacket } from './packet'

const MESSAGE_TYPE = numberToUint16BE(MessageTypes.Subscribe)

export interface SubscribeMessage {
  messageType: number
}

/**
 * Create an outgoing subscribe (0x0000) packet.
 * @param properties The properties for the message.
 * @returns An outgoing subscribe (0x0000) packet.
 */
export function packSubscribeMessage (properties: SubscribeMessage): Uint8Array {
  const data = numberToUint16BE(properties.messageType)
  return packOutgoingPacket({
    messageType: MESSAGE_TYPE,
    data: data
  })
}
