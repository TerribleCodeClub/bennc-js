export const MAX_DATA_LENGTH = 1000;

export const DEFAULT_KEY = new Uint8Array(16);

export enum MessageTypes {
  Subscribe = 0x0000,
  Basic = 0x0001,
  UserDataRequest = 0x0002,
  UserDataResponse = 0x0003,
  Keepalive = 0x0005,
  GetHistory = 0xfffe,
  Unsubscribe = 0xffff,
}
