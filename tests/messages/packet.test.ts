import { packOutgoingPacket, unpackIncomingPacket } from '../../src/messages/packet'

test('Pack an outgoing packet.', () => {
  // Given
  const messageType = new Uint8Array([0x12, 0x34])
  const data = new Uint8Array([0x12, 0x34, 0x56, 0x78])

  // When
  const packedPacket = packOutgoingPacket({
    messageType: messageType,
    data: data
  })

  // Then
  const expectedResult = new Uint8Array([
    // Message type
    0x12, 0x34,
    // Data length
    0x00, 0x04,
    // Data
    0x12, 0x34, 0x56, 0x78
  ])
  expect(packedPacket).toMatchObject(expectedResult)
})

test('Unpack an incoming packet.', () => {
  // Given
  const incomingPacket = new Uint8Array([
    // Message type
    0x12, 0x34,
    // Sender ID
    0xaa, 0xbb, 0xcc, 0xdd,
    // Data length
    0x00, 0x04,
    // Data
    0x12, 0x34, 0x56, 0x78
  ])

  // When
  const unpackedResult = unpackIncomingPacket(incomingPacket)

  // Then
  expect(unpackedResult.messageType).toBe(0x1234)
  expect(unpackedResult.senderId).toBe(0xaabbccdd)
  expect(unpackedResult.data).toMatchObject(new Uint8Array([0x12, 0x34, 0x56, 0x78]))
})
