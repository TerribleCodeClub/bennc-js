import { MessageTypes } from '../../src/common'
import { packers, unpackers } from '../../src/mapping'

const KEY = new Uint8Array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])

test('Create a basic message (0x0001) packet.', () => {
  // Given
  const encoder = new TextEncoder()
  const message = encoder.encode('Hello, World!')

  // When
  const packedPacket = packers[MessageTypes.Basic](
    message,
    KEY
  )

  // Then
  // We can't check the contents of the data as it's encrypted with a random nonce.
  // Check the message type and length.
  expect(packedPacket.slice(0, 4)).toMatchObject(new Uint8Array([0x00, 0x01, 0x00, 0x2D]))

  // Check the total length is as expected.
  expect(packedPacket.length).toBe(49)
})

test('Parse a basic message (0x0001).', () => {
  // Given
  const data = new Uint8Array([1, 2, 3, 4])

  // When
  const unpackedPacket = unpackers[MessageTypes.Basic](data)

  // Then
  expect(unpackedPacket)
  expect(unpackedPacket).toMatchObject(data)
})
