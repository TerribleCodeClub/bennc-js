import { MessageTypes } from '../../src/common'
import { packers } from '../../src/mapping'

test('Create a keepalive (0x0005) packet.', () => {
  // When
  const packedPacket = packers[MessageTypes.Keepalive]()

  // Then
  const expectedResult = new Uint8Array([0x00, 0x05, 0x00, 0x00])
  expect(packedPacket).toMatchObject(expectedResult)
})
