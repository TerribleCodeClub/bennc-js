import { MessageTypes } from '../../src/common'
import { packers } from '../../src/mapping'

test('Create a subscribe (0x0000) packet.', () => {
  // Given
  const messageType = 0xabcd

  // When
  const packedPacket = packers[MessageTypes.Subscribe]({ messageType: messageType })

  // Then
  const expectedResult = new Uint8Array([0x00, 0x00, 0x00, 0x02, 0xab, 0xcd])
  expect(packedPacket).toMatchObject(expectedResult)
})
