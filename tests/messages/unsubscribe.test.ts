import { MessageTypes } from '../../src/common'
import { packers } from '../../src/mapping'

test('Create an unsubscribe (0xffff) packet.', () => {
  // Given
  const messageType = 0xabcd

  // When
  const packedPacket = packers[MessageTypes.Unsubscribe]({ messageType: messageType })

  // Then
  const expectedResult = new Uint8Array([0xff, 0xff, 0x00, 0x02, 0xab, 0xcd])
  expect(packedPacket).toMatchObject(expectedResult)
})
