import Color from 'color'
import { MessageTypes } from '../../src/common'
import { packers, unpackers } from '../../src/mapping'

const KEY = new Uint8Array([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F])

test('Create a user data response (0x0003) packet.', () => {
  // Given
  const username = 'Butlersaurus'
  const colour = Color('#FF4000')
  const clientId = 'Mercury'

  // When
  const packedPacket = packers[MessageTypes.UserDataResponse](
    {
      username: username,
      colour: colour,
      clientId: clientId
    },
    KEY
  )

  // Then
  // We can't check the contents of the data as it's encrypted with a random nonce.
  // Check the message type and length.
  expect(packedPacket.slice(0, 4)).toMatchObject(new Uint8Array([0x00, 0x03, 0x00, 0x67]))

  // Check the total length is as expected.
  expect(packedPacket.length).toBe(107)
})

test('Parse a user data response (0x0003).', () => {
  // Given
  const data = new Uint8Array([
    0, 12,
    66, 117, 116, 108, 101, 114, 115, 97, 117, 114, 117, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    255, 64, 0,
    0, 7,
    77, 101, 114, 99, 117, 114, 121, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  ])
  const username = 'Butlersaurus'
  const colour = Color('#FF4000')
  const clientId = 'Mercury'

  // When
  const unpackedPacket = unpackers[MessageTypes.UserDataResponse](data)

  // Then
  expect(unpackedPacket.username).toBe(username)
  expect(unpackedPacket.colour).toMatchObject(colour)
  expect(unpackedPacket.clientId).toBe(clientId)
})
