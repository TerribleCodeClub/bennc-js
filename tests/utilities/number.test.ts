import { numberToUint16BE, numberToUint32BE } from '../../src/utilities/number'

test('Test number conversion to Uint16 big endian buffer.', () => {
  // When
  const result = numberToUint16BE(1234)

  // Then
  const expectedResult = new Uint8Array([0x04, 0xd2])
  expect(result).toMatchObject(expectedResult)
})

test('Test number conversion to Uint32 big endian buffer.', () => {
  // When
  const result = numberToUint32BE(123456)

  // Then
  const expectedResult = new Uint8Array([0x00, 0x01, 0xE2, 0x40])
  expect(result).toMatchObject(expectedResult)
})
