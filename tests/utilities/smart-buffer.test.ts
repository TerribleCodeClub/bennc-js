import { SmartBuffer } from '../../src/utilities/smart-buffer'

test('Read a UInt16.', () => {
  // Given
  const buffer = [0x30, 0x39]

  // When
  const smartBuffer = SmartBuffer.from(buffer)

  // Then
  expect(smartBuffer.readUInt16()).toBe(12345)
})

test('Read a UInt32.', () => {
  // Given
  const buffer = [0x49, 0x96, 0x02, 0xD2]

  // When
  const smartBuffer = SmartBuffer.from(buffer)

  // Then
  expect(smartBuffer.readUInt32()).toBe(1234567890)
})

test('Read a buffer.', () => {
  // Given
  const buffer = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

  // When
  const smartBuffer = SmartBuffer.from(buffer)

  // Then
  const result = smartBuffer.readBytes(4)
  expect(result).toMatchObject(new Uint8Array([0, 1, 2, 3]))
})

test('Read a UInt16 from an offset.', () => {
  // Given
  const buffer = [0x00, 0x00, 0x30, 0x39]

  // When
  const smartBuffer = SmartBuffer.from(buffer)
  smartBuffer.cursor = 2

  // Then
  expect(smartBuffer.readUInt16()).toBe(12345)
})

test('Read a UInt32 from an offset.', () => {
  // Given
  const buffer = [0x00, 0x00, 0x49, 0x96, 0x02, 0xD2]

  // When
  const smartBuffer = SmartBuffer.from(buffer)
  smartBuffer.cursor = 2

  // Then
  expect(smartBuffer.readUInt32()).toBe(1234567890)
})

test('Read a buffer from an offset.', () => {
  // Given
  const buffer = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

  // When
  const smartBuffer = SmartBuffer.from(buffer)
  smartBuffer.cursor = 2

  // Then
  expect(smartBuffer.readBytes(4)).toMatchObject(new Uint8Array([2, 3, 4, 5]))
})

test('Write a UInt16.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.writeUInt16(12345)

  // Then
  expect(smartBuffer.data).toMatchObject(new Uint8Array([0x30, 0x39]))
})

test('Write a UInt32.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.writeUInt32(1234567890)

  // Then
  expect(smartBuffer.data).toMatchObject(new Uint8Array([0x49, 0x96, 0x02, 0xD2]))
})

test('Write a buffer.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.writeBytes([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

  // Then
  expect(smartBuffer.data).toMatchObject(new Uint8Array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]))
})

test('Write a UInt16 at an offset.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.cursor = 2
  smartBuffer.writeUInt16(12345)

  // Then
  expect(smartBuffer.data).toMatchObject(new Uint8Array([0x00, 0x00, 0x30, 0x39]))
})

test('Write a UInt32 at an offset.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.cursor = 2
  smartBuffer.writeUInt32(1234567890)

  // Then
  expect(smartBuffer.data).toMatchObject(new Uint8Array([0x00, 0x00, 0x49, 0x96, 0x02, 0xD2]))
})

test('Write a buffer at an offset.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.cursor = 2
  smartBuffer.writeBytes([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

  // Then
  expect(smartBuffer.data).toMatchObject(new Uint8Array([0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]))
})

test('Cursor is correctly incremented after reading a UInt16.', () => {
  // Given
  const buffer = new Uint8Array(4)

  // When
  const smartBuffer = SmartBuffer.from(buffer)

  // Then
  smartBuffer.readUInt16()
  expect(smartBuffer.cursor).toBe(2)
})

test('Cursor is correctly incremented after reading a UInt32.', () => {
  // Given
  const buffer = new Uint8Array(4)

  // When
  const smartBuffer = SmartBuffer.from(buffer)

  // Then
  smartBuffer.readUInt32()
  expect(smartBuffer.cursor).toBe(4)
})

test('Cursor is correctly incremented after reading a buffer.', () => {
  // Given
  const buffer = new Uint8Array(8)

  // When
  const smartBuffer = SmartBuffer.from(buffer)

  // Then
  smartBuffer.readBytes(4)
  expect(smartBuffer.cursor).toBe(4)
})

test('Cursor is correctly incremented after writing a UInt16.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.writeUInt16(12345)

  // Then
  expect(smartBuffer.cursor).toBe(2)
})

test('Cursor is correctly incremented after writing a UInt32.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.writeUInt32(1234567890)

  // Then
  expect(smartBuffer.cursor).toBe(4)
})

test('Cursor is correctly incremented after writing a buffer.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.writeBytes([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

  // Then
  expect(smartBuffer.cursor).toBe(10)
})

test('Seek to position below 0 throws range error.', () => {
  // When
  const smartBuffer = new SmartBuffer()

  // Then
  expect(() => {
    smartBuffer.cursor = -1
  }).toThrow(RangeError)
})

test('Pad some data.', () => {
  // Given
  const smartBuffer = new SmartBuffer()

  // When
  smartBuffer.pad(10)

  // Then
  expect(smartBuffer.length).toBe(10)
})
